/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Image,
  StyleSheet,
  View,
  Text,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  return (
    <>
      <View style={{flex:1, backgroundColor: '#ffe4c4' }}>
        <Image source={require('./asset/d.png')} style={styles.image} />
        <Text style={styles.text1}>
          Pink Wallet
        </Text>
        <Text style={styles.text2}>฿120</Text>
        <Text style={styles.text2}>
          This pink and red leather Madras Love wallet
          from Miu Miu features an envelope style,
          gold-tone hardware, a front logo plaque,
          an internal logo stamp, a note compartment
          and three credit card slots.
      </Text>
        <Text style={styles.text3}>Leather 100%, Metal 100%</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },

  body: {
    backgroundColor: '#f0f8ff',
  },
  text1: {
    top: 120,
    left: 30,
    fontSize: 30,
    fontWeight: '600',
    color: '#55514e',
  },
  text2: {
    top: 120,
    left: 30,
    paddingRight: 40,
    fontSize: 20,
    fontWeight: '300',
    color: '#55514e',
  },
  text3: {
    top: 150,
    left: 30,
    paddingRight: 40,
    fontSize: 20,
    fontWeight: '400',
    color: '#55514e',
  },
  image: {
    width: 300,
    height: 300,
    top: 100,
    left: 50
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    color: '#55514e',
    textAlign: 'center'
  },

});

export default App;
